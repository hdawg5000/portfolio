$(function () {
    $(".name").hide().css("color", "white").delay(200).fadeIn(2000);
    $(".delay").delay(1000).fadeIn(2000);
    $(".intro-text li a").hide().css("color", "white").delay(2000).fadeIn(1000);
});